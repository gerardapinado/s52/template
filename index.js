function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.

    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.

    if(letter.length>1){
        // console.log(`Letter is more than 1`)
        return undefined
    }else {
        for(let i=0;i<sentence.length;i++){
            if(sentence[i]===letter){
                result+=1
            }
        }
        return result
    }
    
}


function isIsogram(text) {
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.

    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
    // text.toLowerCase()
    
    // for(let x=0;x<text.length;x++){
    //     let count=0

    //     for(let y=0;y<text.length;y++){
    //         if(text[x].toLowerCase()===text[y].toLowerCase()){
    //             count+=1
    //         }
    //     }
        
    //     if(count>1){
    //         // console.log(count+"false")
    //         return false
    //     }else if(count<=1){
    //         // console.log(count+"true")
    //         return true
    //     }
    // }

    let letter = []

    for(let i=0;i<text.length;i++){
        if(letter.indexOf(text[i]) !== -1){
            return false
        }
        else{
            letter+=text[i]
        }
    }
    return true
}

function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.

    let discountedPrice = price - (price*0.2)
    // console.log(discountedPrice)
    if(age<13){
        return undefined
    }else if(age>=13&&age<=21){
        // console.log(discountedPrice.toFixed(2))
        return discountedPrice.toFixed(2)
    }else if(age>=22&&age<=64){
        // console.log(price.toFixed(2))
        return price.toFixed(2)
    }else {
        // console.log(discountedPrice.toFixed(2))
        return discountedPrice.toFixed(2)
    }
    
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let result = items.map(data => {
        if(data.stocks==0){
            return data.category
        }
    })

    let uniqeResult = result.filter((data,index) => {
        return result.indexOf(data)===index
    })

    console.log(uniqeResult.slice(1))
    return uniqeResult.slice(1)
}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let x,y;
    let result= []
    for(x=0;x<candidateA.length;x++){
        for(y=0;y<candidateA.length;y++){
            if(candidateA[x]==candidateB[y]){
                result.push(candidateB[y])
            }
        }
    }
    return result
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};